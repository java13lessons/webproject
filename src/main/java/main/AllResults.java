package main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;

/**
 * Servlet implementation class AllResults
 */
@WebServlet("/AllResults")
public class AllResults extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Connection con = null;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AllResults() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	private void updateTeam(int teamId, HttpServletRequest request) throws Exception {
		int position = Integer.parseInt(request.getParameter("position[" + teamId + "]"));
		String name = request.getParameter("name[" + teamId + "]");
		int games_played = Integer.parseInt(request.getParameter("games_played[" + teamId + "]"));
		int wins = Integer.parseInt(request.getParameter("wins[" + teamId + "]"));
		int draws = Integer.parseInt(request.getParameter("draws[" + teamId + "]"));
		int loses = Integer.parseInt(request.getParameter("loses[" + teamId + "]"));
		int goalDifference = Integer.parseInt(request.getParameter("goal_diff[" + teamId + "]"));
		int points = Integer.parseInt(request.getParameter("points[" + teamId + "]"));
		PreparedStatement stmt = this.con.prepareStatement("update `league_table` set `team` = ?,`games_played` = ?,"
				+ "`wins` = ?,`draws` = ?," + "`loses` = ?,`goal_difference` = ?,`points` = ?  where `position` = ?");
		stmt.setInt(8, position);
		stmt.setInt(1, teamId);
		stmt.setInt(2, games_played);
		stmt.setInt(3, wins);
		stmt.setInt(4, draws);
		stmt.setInt(5, loses);
		stmt.setInt(6, goalDifference);
		stmt.setInt(7, points);
		stmt.executeUpdate();

		stmt = this.con.prepareStatement("update teams set name = ? where id = ?");
		stmt.setString(1, name);
		stmt.setInt(2, teamId);
		stmt.executeUpdate();
	}

	private void update(HttpServletRequest request) throws Exception {
		ResultSet teams = null;

		Class.forName("com.mysql.cj.jdbc.Driver");
//			this.con.setAutoCommit(false);

		this.con = DriverManager.getConnection("jdbc:mysql://127.0.0.1/league", "root", "");
		teams = this.con.createStatement().executeQuery("SELECT * FROM teams");
		while (teams.next())
			this.updateTeam(teams.getInt("ID"), request);

//			this.con.commit();

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");

			this.con = DriverManager.getConnection("jdbc:mysql://127.0.0.1/league", "root", "");

			if (request.getParameter("save") != null)
				this.update(request);

			this.setTeamsResults(request);
			this.con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		RequestDispatcher view = request.getRequestDispatcher("AllResults.jsp");
		view.forward(request, response);
	}

	private void setTeamsResults(HttpServletRequest request) throws Exception {
		List<LeagueTableRow> leagueTableContent = new ArrayList<LeagueTableRow>();
		Map<Integer, String> teamsMapping = new TreeMap<Integer, String>();
		ResultSet teamsResults = null;
		teamsResults = this.con.createStatement()
				.executeQuery("SELECT * FROM league_table INNER JOIN teams ON teams.ID = league_table.team");

		while (teamsResults.next()) {
			LeagueTableRow teamResult = new LeagueTableRow();
			teamResult.setTeamID(teamsResults.getInt("team"));
			teamResult.setPosition(teamsResults.getInt("position"));
			teamResult.setGamesPlayed(teamsResults.getInt("games_played"));
			teamResult.setWins(teamsResults.getInt("wins"));
			teamResult.setDraws(teamsResults.getInt("draws"));
			teamResult.setLoses(teamsResults.getInt("loses"));
			teamResult.setGoalDiff(teamsResults.getInt("goal_difference"));
			teamResult.setPoints(teamsResults.getInt("points"));
			leagueTableContent.add(teamResult);
			teamsMapping.put(teamResult.getTeamID(), teamsResults.getString("Name"));
		}
		request.setAttribute("League Table", leagueTableContent);
		request.setAttribute("Teams mapping", teamsMapping);

	}

}
