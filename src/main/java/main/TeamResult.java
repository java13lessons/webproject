package main;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;

/**
 * Servlet implementation class TeamResult
 */
@WebServlet("/TeamResult")
public class TeamResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TeamResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.setTeamResult(request);
		RequestDispatcher view = request.getRequestDispatcher("teamResult.jsp");
		view.forward(request, response);
	}

	private void setTeamResult(HttpServletRequest request) {
		Integer teamIdSelected = Integer.parseInt(request.getParameter("teamSelected"));

		Connection con = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");

			con = DriverManager.getConnection("jdbc:mysql://127.0.0.1/league", "root", "");
			PreparedStatement stm = con.prepareStatement("SELECT * FROM league_table where team=?");
			stm.setInt(1, teamIdSelected);
			ResultSet teamResult = stm.executeQuery();
			if (teamResult.next()) {
				request.setAttribute("Position", teamResult.getInt("position"));
				request.setAttribute("Games Played", teamResult.getInt("games_played"));
				request.setAttribute("Wins", teamResult.getInt("wins"));
				request.setAttribute("Draws", teamResult.getInt("draws"));
				request.setAttribute("Loses", teamResult.getInt("loses"));
				request.setAttribute("Goal Difference", teamResult.getString("goal_difference"));
				request.setAttribute("Points", teamResult.getString("points"));
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();

		}

	}

}
