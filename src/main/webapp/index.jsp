<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>

<%
Connection con = null;
ResultSet teams = null;
try {
	Class.forName("com.mysql.cj.jdbc.Driver");

	con = DriverManager.getConnection("jdbc:mysql://127.0.0.1/league", "root", "");
	teams = con.createStatement().executeQuery("SELECT * FROM teams");

} catch (Exception e) {
	e.printStackTrace();
}
%>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="styles/css/bootstrap.min.css">
<meta charset="ISO-8859-1">
<title>Home</title>
</head>
<body>
	<div class="container">
		<form method="POST" action="/MainWebProject/AllResults">
			<div style="padding-bottom: 1em">
				<button class="btn btn-primary">Show all teams results!</button>
			</div>
		</form>
		<form method="POST" action="/MainWebProject/TeamResult">
			<input hidden id="teamSelected" name="teamSelected">
			<%
			while (teams.next()) {
			%>
			<div class="row">
				<div class="col-sm-2">
					<%=teams.getString("Name")%>
				</div>
				<div class="col-sm-2">
					<button id="<%=teams.getInt("Id")%>"
						onclick="document.getElementById('teamSelected').value = <%=teams.getInt("Id")%>;   this.form.submit()"
						type="button">Show team results!</button>
				</div>
			</div>
			<hr>
			<%
			}
			%>
		</form>
	</div>
</body>
</html>
<%
if (con != null)
	con.close();
%>