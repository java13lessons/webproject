<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="styles/css/bootstrap.min.css">
<meta charset="ISO-8859-1">
<title>Team Result</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-sm-2">
				<b>Position:</b>
			</div>
			<div class="col-sm-2">
				<%=request.getAttribute("Position")%>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-2">
				<b>Games played:</b>
			</div>
			<div class="col-sm-2">
				<%=request.getAttribute("Games Played")%>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-2">
				<b>Wins:</b>
			</div>
			<div class="col-sm-2">
				<%=request.getAttribute("Wins")%>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-2">
				<b>Draws:</b>
			</div>
			<div class="col-sm-2">
				<%=request.getAttribute("Draws")%>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-2">
				<b>Loses:</b>
			</div>
			<div class="col-sm-2">
				<%=request.getAttribute("Loses")%>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-2">
				<b>Goal difference:</b>
			</div>
			<div class="col-sm-2">
				<%=request.getAttribute("Goal Difference")%>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-2">
				<b>Points:</b>
			</div>
			<div class="col-sm-2">
				<%=request.getAttribute("Points")%>
			</div>
		</div>
	</div>
</body>
</html>