<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Iterator"%>
<%@page import="main.LeagueTableRow"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Map"%>
<%
List<LeagueTableRow> tableList = (List<LeagueTableRow>) request.getAttribute("League Table");
Map<Integer, String> teamsMapping = (Map<Integer, String>) request.getAttribute("Teams mapping");
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="styles/css/bootstrap.min.css">
<title>All Results</title>
</head>
<body>
	<div class="container">
		<form method="POST">
			<table class="table">
				<thead>
					<tr>
						<th>Position</th>
						<th>Name</th>
						<th>Games played</th>
						<th>Wins</th>
						<th>Draws</th>
						<th>Loses</th>
						<th>Goal Difference</th>
						<th>Points</th>
					</tr>
				</thead>
				<tbody>
					<%
					Iterator<LeagueTableRow> iterator = tableList.iterator();
					while (iterator.hasNext()) {
						LeagueTableRow currentEntry = iterator.next();
					%>
					<tr>
						<td><input id="<%=currentEntry.getTeamID()%>_position"
							name="position[<%=currentEntry.getTeamID()%>]" type="number"
							readonly size="" value="<%=currentEntry.getPosition()%>"></td>
						<td><input id="<%=currentEntry.getTeamID()%>_name"
							type="text" name="name[<%=currentEntry.getTeamID()%>]"
							type="text" readonly
							value="<%=teamsMapping.get(currentEntry.getTeamID())%>"></td>
						<td><input id="<%=currentEntry.getTeamID()%>_games_played"
							name="games_played[<%=currentEntry.getTeamID()%>]" type="number"
							readonly value="<%=currentEntry.getGamesPlayed()%>"></td>
						<td><input id="<%=currentEntry.getTeamID()%>_wins"
							name="wins[<%=currentEntry.getTeamID()%>]" type="number" readonly
							value="<%=currentEntry.getWins()%>"></td>
						<td><input id="<%=currentEntry.getTeamID()%>_draws"
							name="draws[<%=currentEntry.getTeamID()%>]" type="number"
							readonly value="<%=currentEntry.getDraws()%>"></td>
						<td><input id="<%=currentEntry.getTeamID()%>_loses"
							name="loses[<%=currentEntry.getTeamID()%>]" type="number"
							readonly value="<%=currentEntry.getLoses()%>"></td>
						<td><input id="<%=currentEntry.getTeamID()%>_goal_diff"
							name="goal_diff[<%=currentEntry.getTeamID()%>]" type="number"
							readonly value="<%=currentEntry.getGoalDiff()%>"></td>
						<td><input id="<%=currentEntry.getTeamID()%>_points"
							name="points[<%=currentEntry.getTeamID()%>]" type="number"
							readonly value="<%=currentEntry.getPoints()%>"></td>
					</tr>
					<%
					}
					%>
				</tbody>
			</table>
			<button name="save" class="btn btn-success">Save</button>
		</form>
		<button class="btn btn-secondary" onclick="setAllEditable()">Change</button>
	</div>
	<style>
input {
	width: 100%
}
</style>
	<script type="text/javascript">
		var teamsIds = [];
		setIds();

		function setIds() {
	<%Iterator<Entry<Integer, String>> iteratorTeams = teamsMapping.entrySet().iterator();
while (iteratorTeams.hasNext()) {
	Entry<Integer, String> currentTeam = iteratorTeams.next();%>
	teamsIds[<%=currentTeam.getKey()%>] = "<%=currentTeam.getValue()%>";
	<%}%>
		}
		function setAllEditable() {
			var keys = Object.keys(teamsIds);
			for (var i = 0; i < keys.length; i++) {
				document.getElementById(keys[i] + "_position").readOnly = false;
				document.getElementById(keys[i] + "_name").readOnly = false;
				document.getElementById(keys[i] + "_games_played").readOnly = false;
				document.getElementById(keys[i] + "_wins").readOnly = false;
				document.getElementById(keys[i] + "_draws").readOnly = false;
				document.getElementById(keys[i] + "_loses").readOnly = false;
				document.getElementById(keys[i] + "_goal_diff").readOnly = false;
				document.getElementById(keys[i] + "_points").readOnly = false;
			}
		}
	</script>
</body>
</html>