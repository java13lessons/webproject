<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="styles/css/bootstrap.min.css">
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%
	int indx = 0;
	final int NUMBER_OF_ROWS = 10, NUMBER_OF_COLUMNS = 20;
	%>
	<table border="1">
		<%
		for (int i = 0; i < NUMBER_OF_ROWS; i++) {
		%>
		<tr>
			<%
			for (int j = 0; j < NUMBER_OF_COLUMNS; j++) {
			%>
			<%
			indx++;
			%>
			<%
			if (indx % 2 == 0) {
			%>
			<td style="color: aqua;"><%=indx%></td>
			<%
			} else {
			%>
			<td style="color: yellow;"><%=indx%></td>
			<%
			}
			%>
			<%
			}
			%>
		</tr>
		<%
		}
		%>
	</table>
</body>
</html>